var express  = require('express'),
	Email    = require('../models/Email'),
	Template = require('../models/Template');

module.exports = (function ()
{
	var router = express.Router();

	router.get('/:id', function (req, res)
	{	
		if (!req.user)
		{
			return res.send(401);
		}

		var id     = req.params.id,
			fs     = require('fs'),
			query  = (req.user.isAdmin)? {_id : id} : {_id : id, userid : req.user.userid}; // If is admin bypass verifying the userid matches

		Email.findOne(query, function (err, email)
		{	
			if (err)
			{
				return res.json(err);
			}

			if (!email)
			{
				return res.json({success : false});
			}

			var templateName = email.template;

			Template.findOne({name : templateName}, function (err, template)
			{
				if (err)
				{
					return res.json(err);
				}

				return res.json({success : true, email : email, availableModules : template.modules})
			});
		})
	});

	router.post('/:id/save', function (req, res)
	{	
		if (!req.user)
		{
			return res.send(401);
		}

		var id      = req.params.id,
			modules = req.body.modules;

		Email.findOne({_id : id, userid : req.user.userid}, function (err, email)
		{
			if (err)
			{
				return res.json(err);
			}

			if (!email)
			{
				return res.send(401);
			}

			email.modules = [];
			email.modules = modules;

			email.save(function (err)
			{
				if (err)
				{
					return res.json(err);
				}

				return res.json({success : true});
			})
		});
	});

	router.post('/:id/export', function (req, res)
	{	
		if (!req.user)
		{
			return res.send(401);
		}

		var id         = req.params.id,
			fs         = require('fs'),
			modules    = req.body.modules,
			cheerio    = require('cheerio');

		var $completeHTML = '';

		var $header = fs.readFileSync('app/templates/test/header.html', 'utf-8');

		$completeHTML += $header;

		$completeHTML += modules.map(function (module)
		{
			return module.html;

		}).join('\n');

		var $footer = fs.readFileSync('app/templates/test/footer.html', 'utf-8');

		$completeHTML += $footer;

		fs.writeFile('files/email_' + id + '.html', $completeHTML, function (err)
		{
			if (err)
			{
				res.json(err);
			}

			res.json({success : true, html : $completeHTML, fileName : 'email_' + id + '.html'});
		});
	});

	router.post('/:id/download', function (req, res)
	{
		if (!req.user)
		{
			return res.send(401);
		}

		var id       = req.params.id,
			fs       = require('fs'),
			modules  = req.body.modules,
			template = req.body.template,
			archiver = require('archiver'),
			output   = fs.createWriteStream('app/temp_files/' + id + '.zip'),
			archive  = archiver('zip'),
			cheerio  = require('cheerio');

		var $completeHTML = '';

		var $header = fs.readFileSync('app/templates/' + template + '/header.html', 'utf-8');

		$completeHTML += $header;

		$completeHTML += modules.map(function (module)
		{	
			var $ = cheerio.load(module.html);
			
			$('img').each(function ()
			{
				var $this  = $(this),
					srcArr = $this.attr('src').split('/'),
					newSrc = 'assets/' + srcArr[srcArr.length -1];

				$this.attr('src', newSrc);
			});

			return $.html();

		}).join('\n');

		var $footer = fs.readFileSync('app/templates/' + template + '/footer.html', 'utf-8');

		$completeHTML += $footer;

		fs.writeFile('app/temp_files/email_' + id + '.html', $completeHTML, function (err)
		{
			if (err)
			{
				res.json(err);
			}

			archive.pipe(output);

			archive.bulk([
			  {cwd: 'public/assets/email-images/' + id, src: ['*.jpg'], dest: 'assets/', expand : true},
			  {cwd: 'app/temp_files', src: ['email_' + id + '.html'], dest: '/', expand : true}
			]);

			archive.on('end', function ()
			{	
				fs.unlink('app/temp_files/email_' + id + '.html');
				return res.json({success : true, id : id});
			});

			archive.finalize();
		});
	});

	router.delete('/:id', function (req, res)
	{	
		if (!req.user)
		{
			return res.send(401);
		}

		Email.remove({_id : req.params.id, userid : req.user.userid}, function (err)
		{
			if (err)
			{
				res.json(err);
			}

			Email.find({userid : req.user.userid}, function (err, emails)
			{
				if (err)
				{
					res.json(err);
				}

				res.json({currentEmails : emails});
			});
		});		
	});

	return router;

}());
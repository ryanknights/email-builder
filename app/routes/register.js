"use strict"

var express = require('express'),
	User    = require('../models/User');

module.exports = (function (app)
{
	var api = express.Router();

	api.post('/', function (req, res)
	{
		var username = req.body.username || '',
			email    = req.body.email || '',
			password = req.body.password || '';

		if (username === '' || password === '', email === '')
		{
			return res.send(401);
		}

		User.findOne({email : email}, function (err, user)
		{
			if (err)
			{
				console.log(err);
				return res.send(401);
			}

			if (user)
			{
				console.log('Email Exists');
				return res.send(401);
			}

			var newUser = new User({username : username, email : email, password : password});

			newUser.save(function (err)
			{	
				if (err)
				{
					return res.send(401);
				}
				
				res.json({success : true});
			});
		});
	})

	return api;

})();
var express  = require('express'),
	Email    = require('../../models/Email'),
	User     = require('../../models/User')

module.exports = (function ()
{
	var router = express.Router();

	router.use(function (req, res, next)
	{
		if (!req.user || !req.user.isAdmin)
		{
			return res.send(403);
		}

		next();
	});

	router.get('/', function (req, res)
	{
		User.find(function (err, users)
		{
			if (err)
			{
				res.send(401);
			}
			
			res.json({users : users});
		});
	});

	router.get('/:id', function (req, res)
	{
		var id = req.params.id;

		User.findOne({_id : id}, function (err, user)
		{	
			if (err)
			{
				return res.send(401);
			}

			if (!user)
			{
				return res.json({success : false, error : 'User Does Not Exist'});
			}

			Email.find({userid : id}, function (err, emails)
			{
				if (err)
				{
					return res.send(401);
				}

				return res.json({user : user, emails : emails});
			});
		})
	});	

	return router;

}());
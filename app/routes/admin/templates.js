var express    = require('express'),
	Template   = require('../../models/Template');

module.exports = (function ()
{
	var router = express.Router();

	router.use(function (req, res, next)
	{
		if (!req.user || !req.user.isAdmin)
		{
			return res.send(403);
		}

		next();
	});

	router.get('/', function (req, res)
	{	
		var fs = require('fs');
		Template.find(function (err, templates)
		{
			if (err)
			{
				res.send(err);
			}

			fs.readdir('app/templates', function (err, folders)
			{
				res.json({currentTemplates : templates, availableTemplates : folders});
			});
		});
	});

	router.post('/', function (req, res)
	{
		var templateName = req.body.template,
			fs           = require('fs');

		Template.find({name : templateName}, function (err, template)
		{
			if (!template.length)
			{
				var newTemplate = new Template();

				newTemplate.name    = templateName;
				newTemplate.modules = [];

				fs.readdir('app/templates/' + templateName + '/modules', function (err, files)
				{
					if (err)
					{
						res.json(err);
					}

					files.forEach(function (file, index)
					{	
						var fileName = file.split('.')[0],
							fileName = fileName.replace('_', ' ');

						newTemplate.modules.push(
						{
							name : fileName,
							html : fs.readFileSync('app/templates/'+ templateName +'/modules/' + file, 'utf-8')
						});
					});

					newTemplate.save(function (err)
					{
						Template.find(function (err, templates)
						{
							if (err)
							{
								res.json(err);
							}

							res.json({currentTemplates : templates});
						});
					});					
				})
			}
			else
			{
				res.json({error : 'template exists'});
			}
		});
	});

	router.get('/:name', function (req, res)
	{
		Template.findOne({name : req.params.name}, function (err, template)
		{
			if (err)
			{
				res.send(err);
			}

			res.json({template : template});
		});
	});

	router.get('/:name/update', function (req, res)
	{
		var fs = require('fs');

		Template.findOne({name : req.params.name}, function (err, template)
		{
			fs.readdir('app/templates/' + req.params.name + '/modules', function (err, files)
			{
				if (err)
				{
					res.json(err);
				}

				template.modules = [];

				files.forEach(function (file, index)
				{	
					var fileName = file.split('.')[0],
						fileName = fileName.replace('_', ' ');

					template.modules.push(
					{
						name : fileName,
						html : fs.readFileSync('app/templates/'+ req.params.name +'/modules/' + file, 'utf-8')
					});
				});

				template.save(function (err, savedTemplate)
				{
					if (err)
					{
						res.json(err);
					}

					res.json({template : savedTemplate});
				});				
			});
		});
	});

	router.delete('/:name', function (req, res)
	{
		Template.remove({name : req.params.name}, function (err)
		{
			if (err)
			{
				res.json(err);
			}

			res.json({success : true});
		})
	});

	return router;
}())
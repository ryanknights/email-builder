"use strict"

var express = require('express'),
	User    = require('../models/User');

module.exports = (function (app)
{
	var api = express.Router();

	api.get('/', function (req, res)
	{
		if (!req.user)
		{
			return res.send(401);
		}

		User.findOne({_id: req.user.userid}, function (err, user)
		{
			if (err)
			{
				return res.send(401);
			}

			return res.json({user : {username : user.username, userid : user._id, isAdmin : user.isAdmin}});
		});
	});

	return api;

})();
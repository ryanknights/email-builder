var express  = require('express'),
	Email    = require('../models/Email'),
	Template = require('../models/Template'),
	User     = require('../models/User');

module.exports = (function ()
{
	var router = express.Router();

	router.get('/', function (req, res)
	{	
		if (!req.user)
		{
			return res.send(401);
		}

		Email.find({userid: req.user.userid}, function (err, emails)
		{
			if (err)
			{
				res.json(err);
			}

			Template.find(function (err, templates)
			{
				if (err)
				{
					res.json(err);
				}

				res.json({currentEmails : emails, availableTemplates : templates});
			});
		})
	});

	router.post('/', function (req, res)
	{	
		if (!req.user)
		{
			return res.send(401);
		}

		var newEmail = new Email(
		{
			userid    : req.user.userid,
			name      : req.body.email.name,
			template  : req.body.email.template
		});

		newEmail.save(function (err)
		{
			if (err)
			{
				res.send(401);
			}

			Email.find({userid: req.user.userid}, function (err, emails)
			{
				if (err)
				{
					res.json(err);
				}

				res.json({currentEmails : emails});
			})
		})
	});

	return router;

}());
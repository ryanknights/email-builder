'use strict';

var express     = require('express'),
	multiparty  = require('multiparty'),
	fs          = require('fs');


module.exports = (function ()
{
	var router = express.Router();

	router.post('/', function (req, res)
	{
		var form = new multiparty.Form();

		form.parse(req, function (err, fields, files)
		{	
			var emailId  = fields.emailId[0],
				tempImg  = files.file[0].path,
				fileName = files.file[0].originalFilename;

			fs.mkdir('public/assets/email-images/' + emailId, function (err)
			{	
				var newImg = 'public/assets/email-images/' + emailId + '/' + fileName;

				// http://stackoverflow.com/questions/4568689/how-do-i-move-file-a-to-a-different-partition-or-device-in-node-js
				var is = fs.createReadStream(tempImg);
				var os = fs.createWriteStream(newImg);

				is.pipe(os);
				is.on('end',function() {
				    fs.unlinkSync(tempImg);
				    return res.json({img : '/assets/email-images/' + emailId + '/' + fileName});
				});

				// fs.rename(tempImg, newImg, function (err)
				// {
				// 	if (err)
				// 	{
				// 		return res.json(err);
				// 	}

				// 	return res.json({img : '/assets/email-images/' + emailId + '/' + fileName});
				// });				
			});
		});
	});

	return router;
}());
"use strict"

var express = require('express'),
	jwt     = require('jsonwebtoken');

module.exports = (function (app)
{
	var api = express.Router();

	api.get('/file/:id', function (req, res)
	{
		var filePath = process.cwd() + '/app/temp_files/' + req.params.id + '.zip',
			fs       = require('fs');

		var stream = fs.createReadStream(filePath, {bufferSize: 64 * 1024});
		   stream.pipe(res);

		   var had_error = false;
		   stream.on('error', function(err){
		      had_error = true;
		   });
		   stream.on('close', function(){
		   	console.log('Stream Close');
		   if (!had_error) fs.unlink(filePath);
			});
	});

	api.get('/*', function (req, res)
	{
		res.sendfile('./public/index.html');
	})

	return api;

})();
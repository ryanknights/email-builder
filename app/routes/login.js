"use strict"

var express   = require('express'),
	User      = require('../models/User'),
	jwt       = require('jsonwebtoken'),
	jwtSecret = require('../config/secret');

module.exports = (function (app)
{
	var api = express.Router();

	api.post('/', function (req, res)
	{
		var username = req.body.username || '',
			password = req.body.password || '';

		if (username === '' || password === '')
		{
			return res.send(401);
		}

		User.findOne({username : username}, function (err, user)
		{
			if (err)
			{
				console.log(err);
				return res.send(401);
			}

			if (!user)
			{
				console.log('No user found');
				return res.send(401);
			}

			user.comparePassword(password, function (isMatch)
			{
				if (!isMatch)
				{
					console.log('Attempt failed to login with ' + user.username);
					return res.send(401);
				}

				var token = jwt.sign({userid: user._id, isAdmin: user.isAdmin}, jwtSecret.secret, {expiresInMinutes : 60});

				return res.json({user : {username : user.username, userid : user._id, isAdmin : user.isAdmin}, token : token});
			});
		});
	})

	return api;

})();
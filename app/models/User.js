var mongoose = require('mongoose'),
	bcrypt   = require('bcrypt');

var userSchema = mongoose.Schema(
{
	username :
	{
		type     : String,
		required : true,
		unique   : true
	},
	email :
	{
		type     : String,
		required : true,
		unique   : true,
	},
	password :
	{
		type     : String,
		required : true
	},
	created :
	{
		type    : Date,
		default : Date.now
	},
	isAdmin :
	{
		type    : Boolean,
		default : false
	}
});

userSchema.pre('save', function (next)
{
	var user = this;

	if (!user.isModified('password'))
	{
		return next();
	}

	bcrypt.genSalt(10, function (err, salt)
	{
		if (err)
		{
			return next(err);
		}

		bcrypt.hash(user.password, salt, function (err, hash)
		{
			if (err)
			{
				return next(err);
			}

			user.password = hash;
			next();
		});
	});
});

userSchema.methods.comparePassword = function (password, callback)
{
	bcrypt.compare(password, this.password, function (err, isMatch)
	{
		if (err)
		{
			return callback(err);
		}

		callback(isMatch);
	});
}

module.exports = mongoose.model('User', userSchema);
var mongoose = require('mongoose');

var emailSchema = mongoose.Schema(
{	
	userid    : String,
	name      : String,
	template  : String,
	modules   :
	[
		{
			name : String,
			html : String
		}
	]
});

module.exports = mongoose.model('Email', emailSchema);
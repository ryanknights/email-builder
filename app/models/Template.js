var mongoose = require('mongoose');

var templateSchema = mongoose.Schema(
{
	name      : String,
	modules   :
	[
		{
			name : String,
			html : String
		}
	]
});

module.exports = mongoose.model('Template', templateSchema);
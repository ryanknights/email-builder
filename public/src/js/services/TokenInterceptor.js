'use strict'

angular.module('emailBuilder').factory('TokenInterceptor', TokenInterceptor);

function TokenInterceptor ($q, $rootScope, $window, $location, CurrentUserService)
{
	var tokenInterceptor = {};

	tokenInterceptor.request = function (config)
	{
		config.headers = config.headers || {};

		if ($window.sessionStorage.token)
		{
			config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
		}

		return config;
	}

	tokenInterceptor.responseError = function (rejection)
	{	
		if (rejection != null && rejection.status === 401 && ($window.sessionStorage.token || CurrentUserService.user))
		{
			CurrentUserService.destroy();

			$rootScope.$broadcast('auth-not-authenticated');

			$location.path('/login');
		}

		if (rejection != null && rejection.status === 403 && ($window.sessionStorage.token || CurrentUserService.user))
		{	
			$location.path('/');
			$rootScope.$broadcast('auth-not-authorized');
		}		

		return $q.reject(rejection);
	}

	return tokenInterceptor;
}
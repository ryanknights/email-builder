"use strict"

angular.module('emailBuilder').factory('AuthService', AuthService);

function AuthService ($window, $rootScope, $http, CurrentUserService)
{
	var authService = {};

	authService.isLoggedIn = function ()
	{
		return (CurrentUserService.user !== null);
	}

	authService.isAdmin = function ()
	{
		return (CurrentUserService.user.isAdmin === true);
	}

	authService.logIn = function (username, password)
	{
		return $http.post('/api/login', {username : username, password : password})

					.then(function (res)
					{	
						CurrentUserService.setUser(res.data.user, res.data.token);
						
						$rootScope.$broadcast('auth-login-success');

					}, function ()
					{
						$rootScope.$broadcast('auth-login-failed');
					});
	}

	authService.logOut = function ()
	{
		CurrentUserService.destroy();

		$rootScope.$broadcast('auth-logout-success');
	}	

	authService.reAuthenticate = function ()
	{
		return $http.get('/api/authenticate')

				.then(function (res)
				{
					CurrentUserService.setUser(res.data.user);

					$rootScope.$broadcast('auth-login-reauthenticate-success');

					return res.data.user;
				});
	}

	return authService;
}


'use strict'

angular.module('emailBuilder').factory('AccountService', AccountService);

function AccountService ($http)
{	
	var accountService = {};

	accountService.register = function (username, email, password)
	{
		return $http.post('/api/register', {username : username, email : email, password : password});
	}

	return accountService;
}
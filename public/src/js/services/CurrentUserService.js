"use strict"

angular.module('emailBuilder').factory('CurrentUserService', CurrentUserService);

function CurrentUserService ($window)
{
	var CurrentUserService = {};

	CurrentUserService.user = null;

	CurrentUserService.setUser = function (user, token)
	{	
		if (user === undefined)
		{
			throw new Error('setUser requires a valid user object');
			return false;
		}

		this.user = {};

		this.user.id      = user.userid;
		this.user.name    = user.username;
		this.user.isAdmin = user.isAdmin;

		if (token !== undefined)
		{
			$window.sessionStorage.token = token;
		}
	}

	CurrentUserService.getUser = function ()
	{
		return (this.user !== null)? this.user : false;
	}

	CurrentUserService.destroy = function ()
	{
		this.user = null;

		delete $window.sessionStorage.token;
	}

	return CurrentUserService;
}


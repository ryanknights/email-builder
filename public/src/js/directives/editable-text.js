"use strict"

angular.module('emailBuilder').directive('editableText', editText);

function editText ($filter)
{
    return {
        restrict: 'A',
        link: function (scope, element, attr) 
        {
            tinymce.init({
                plugins : 'link textcolor',
                force_br_newlines : true,
                force_p_newlines : false,
                forced_root_block : '',           
                selector: "[editable-text]",
                inline: true,
                toolbar: "bold underline italic | link | forecolor backcolor |",
                menubar: false,
                setup : function (ed)
                {
                    ed.on('blur', function ()
                    {
                        scope.module.html = $filter('cleanHTML')(element.closest('div.view'));
                        scope.$apply();
                    });
                }
            });
        }
    };  
}
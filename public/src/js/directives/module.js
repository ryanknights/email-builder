"use strict"

angular.module('emailBuilder').directive('module', moduleDirective);

function moduleDirective ($compile, $parse)
{
    return {
        restrict: 'A',
        link: function (scope, element, attr) 
        {
            element.prepend('<ul id="module-controls"><li><span class="label label-primary" data-handle>Handle</span></li><li><span class="label label-danger" data-delete>Delete</span></li></ul>');

            var delBtn = element.find('[data-delete]');

            scope.$watch(attr.content, function () 
            {
                element.children('.view').html($parse(attr.content)(scope));
                $compile(element.contents())(scope);

            }, true);

            delBtn.on('click', function ()
            {
                var index = scope.currentModules.indexOf(scope.module);
                scope.currentModules.splice(index, 1);

                scope.$apply();
            });
        }
    };  
}
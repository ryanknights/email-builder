"use strict"

angular.module('emailBuilder').directive('editableImage', editImage);

function editImage ($compile, $upload, $filter)
{   
    console.log('Editable Image Directive Initialised');

    return {
        restrict: 'A',
        scope   : true,
        link: function (scope, element, attr)
        {   
            console.log(element);
            element.before($compile('<span class="label label-primary" data-remove-me data-image-upload><input type="file" ng-file-select="upload(files)" ng-model="files">Upload Image</span>')(scope));

            scope.upload = function (files) 
            {
                if (files && files.length) 
                {
                    for (var i = 0; i < files.length; i++) 
                    {
                        var file = files[i];
                        $upload.upload(
                        {
                            url: 'api/image-upload',
                            fields: {emailId : scope.$parent.email._id},
                            file: file

                        }).success(function (data, status, headers, config) 
                        {   
                            console.log(element);
                            element.attr('src', data.img);
                            scope.module.html = $filter('cleanHTML')(element.closest('div.view'));
                        });
                    }
                }
            };

        }
    }
}
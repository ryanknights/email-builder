"use strict"

angular.module('emailBuilder').directive('editableBackground', editBackground);

function editBackground ($filter)
{   
    console.log('Editable Background Directive Initialised');

    return {
        restrict: 'A',
        link: function (scope, element, attr)
        {   
            element.prepend('<span class="label label-primary" data-remove-me data-background>Change BG</span>');

            var bgBtn = element.children('span[data-background]');

            bgBtn.ColorPicker(
            {
                onChange : function (hsb, hex, rgb)
                {
                    element.css('background-color', '#' + hex);                   
                },
                onHide : function ()
                {
                    scope.module.html = $filter('cleanHTML')(element.closest('div.view'));
                    scope.$apply();                     
                }  
            });
        }
    }
}
angular.module('emailBuilder').filter('cleanHTML', function ()
{
	return function (module)
	{	
		var clone = module.clone();

		var editableText = clone.find('[editable-text]');

		clone.find('[data-remove-me]').remove()
		editableText.removeAttr('id');
		editableText.removeAttr('contenteditable spellcheck');
		editableText.removeClass('mce-edit-focus mce-content-body');
		editableText.css('position', '');

		return clone.html();
	}
});
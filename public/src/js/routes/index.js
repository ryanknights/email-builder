"use strict"

angular.module('emailBuilder').config(Routes);

function Routes ($routeProvider, $locationProvider)
{
	$routeProvider

		.when('/',
		{
			controller  : 'emailsCtrl',
			templateUrl : '/views/emails.html',
			access      : { requiredLogin: true }
		})

		.when('/login',
		{
			controller  : 'loginCtrl',
			templateUrl : '/views/login.html',
			access      : { requiredLogin: false} 
		})
		.when('/register',
		{
			controller  : 'registerCtrl',
			templateUrl : '/views/register.html',
			access      : { requiredLogin: false}
		})
		.when('/admin/templates',
		{
			controller : 'templatesCtrl',
			templateUrl : '/views/templates.html',
			access      : { requiredLogin: true, isAdmin : true },
			resolve     :
			{
				Templates : function ($q, $http)
				{
					var deferred = $q.defer();

					$http.get('api/admin/templates')

						.success(function (data)
						{
							deferred.resolve(data);
						});

					return deferred.promise;
				}
			}
		})

		.when('/admin/templates/:name', 
		{
			controller : 'templateCtrl',
			templateUrl : '/views/template.html',
			access      : { requiredLogin: true, isAdmin : true },
			resolve     :
			{
				Template : function ($q, $http, $route)
				{
					var deferred = $q.defer();

					$http.get('api/admin/templates/' + $route.current.params.name)

						.success(function (data)
						{
							deferred.resolve(data.template);
						});

					return deferred.promise;
				}
			}
		})

		.when('/admin/users',
		{
			controller : 'usersCtrl',
			templateUrl : '/views/users.html',
			access      : { requiredLogin: true, isAdmin : true},
			resolve     :
			{
				Users : function ($q, $http, $route)
				{
					var deferred = $q.defer();

					$http.get('api/admin/users')

						.success(function (data)
						{
							deferred.resolve(data.users);
						});

					return deferred.promise;
				}
			}
		})

		.when('/admin/users/:id',
		{
			controller : 'userCtrl',
			templateUrl : '/views/user.html',
			access      : { requiredLogin: true, isAdmin : true},
			resolve     :
			{
				User : function ($q, $http, $route)
				{
					var deferred = $q.defer();

					$http.get('api/admin/users/' + $route.current.params.id)

						.success(function (data)
						{
							deferred.resolve(data);
						});

					return deferred.promise;
				}
			}			
		})

		.when('/email/:id',
		{
			controller : 'emailCtrl',
			templateUrl : '/views/email.html',
			access      : { requiredLogin: true },
			resolve     :
			{
				currentEmail : function ($q, $http, $route, $location)
				{
					var deferred = $q.defer();

					$http.get('api/email/' + $route.current.params.id)

						.success(function (data)
						{	
							if (!data.success)
							{	
								alert('Incorrect UserID');
								return $location.path('/');
							}

							deferred.resolve(data);
						});

					return deferred.promise;
				}
			}			
		})

		.otherwise(
		{
			redirectTo : '/'
		});
}
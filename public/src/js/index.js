"use strict"

angular.module('emailBuilder', ['ngRoute', 'ui.sortable', 'angularFileUpload']);

angular.module('emailBuilder').config(function ($httpProvider)
{
	$httpProvider.interceptors.push('TokenInterceptor');
});

angular.module('emailBuilder').run(function ($rootScope, $http, $location, $window, AuthService, CurrentUserService)
{	
	function routeAuthentication ()
	{	
		$rootScope.$on('$routeChangeStart', function (event, nextRoute, currentRoute)
		{
	        if (nextRoute.access && nextRoute.access.requiredLogin && !AuthService.isLoggedIn()) 
	        {
				CurrentUserService.destroy();

				$rootScope.$broadcast('auth-not-authenticated');  

	            return $location.path('/login');
	        }

	        if (nextRoute.access && nextRoute.access.isAdmin && !AuthService.isAdmin())
	        {
	        	$rootScope.$broadcast('auth-not-authorized');

	        	return $location.path('/');
	        }
		});		
	}

	if (window.sessionStorage.token) // If we have a token stored we assume a user has been logged in
	{
		AuthService.reAuthenticate() // Reauthenticate the user 
			.finally(routeAuthentication); // Then regardless of the outcome, add the route authentication functions above in case we are on a private/admin page when loading,
										   // in which case our CurrentUserService that AuthService methods uses will not be filled until the reAuthenticate has resolved. 
	}
	else // No token so no need to wait to add the route authentication
	{
		routeAuthentication();
	}
});

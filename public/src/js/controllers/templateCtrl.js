"use strict"

angular.module('emailBuilder').controller('templateCtrl', templateCtrl);

function templateCtrl ($scope, $http, $routeParams, $location, Template)
{
	$scope.template = Template;

	$scope.deleteTemplate = function ()
	{
		$http.delete('/api/admin/templates/' + $routeParams.name)

			.success(function (data)
			{
				if (data.success)
				{
					$location.path('/templates');
				}
				else
				{
					alert('Error');
				}
			})
			.error(function (data)
			{
				console.log(data);
			});
	};

	$scope.updateTemplate = function ()
	{
		$http.get('/api/admin/templates/' + $routeParams.name + '/update')

			.success(function (data)
			{
				$scope.template = data.template;
				
				alert('Template Successfuly Rescanned & Updated');
			})
			.error(function (data)
			{
				console.log(data);
			});
	};
}
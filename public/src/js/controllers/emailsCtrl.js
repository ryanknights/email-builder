"use strict"

angular.module('emailBuilder').controller('emailsCtrl', emailsCtrl);

function emailsCtrl ($scope, $http)
{
	$scope.emails    = {};
	$scope.templates = {};

	$http.get('api/emails')

		.success(function (data)
		{
			$scope.emails    = data.currentEmails;
			$scope.templates = data.availableTemplates;
		})
		.error(function (data)
		{
			console.log(data);
		});

	$scope.addEmail = function ()
	{
		$http.post('api/emails', {email : $scope.newEmail})

			.success(function (data)
			{
				$scope.emails = data.currentEmails
			})
			.error(function (data)
			{
				console.log(data);
			})
	};

	$scope.deleteEmail = function (id)
	{
		$http.delete('api/email/' + id)

			.success(function (data)
			{
				$scope.emails = data.currentEmails;
			})
			.error(function (data)
			{
				console.log(data);
			})
	}


}
'use strict'

angular.module('emailBuilder').controller('userCtrl', userCtrl);

function userCtrl ($scope, User)
{
	$scope.user   = User.user;
	$scope.emails = User.emails; 
}
"use strict"

angular.module('emailBuilder').controller('emailCtrl', emailCtrl);

function emailCtrl ($scope, $http, $routeParams, $window, $upload, currentEmail)
{	
	console.log(currentEmail.email);

	$scope.email   = currentEmail.email;
	$scope.modules = currentEmail.availableModules;

	$scope.currentModules        = currentEmail.email.modules;
	$scope.currentModulesOptions = {handle : '[data-handle]', appendTo : 'body'};

	$scope.availableModules = angular.copy($scope.modules, []);
	$scope.availableModulesOptions =
	{	
		appendTo : 'body',
		helper   : 'clone',
		connectWith : '[data-email-container]',
		placeholder: "sortable-placeholder",
	    stop: function (e, ui) {
	      // if the element is removed from the first container
	      if ($(e.target).hasClass('first') &&
	          ui.item.sortable.droptarget &&
	          e.target != ui.item.sortable.droptarget[0]) {
	        // clone the original model to restore the removed item
	        $scope.availableModules = angular.copy($scope.modules, []);
	      }
	    }			
	};

	$scope.clear = function ()
	{
		$scope.currentModules = [];
	};

	$scope.export = function ()
	{
		$http.post('api/email/' + $routeParams.id + '/export', {modules : $scope.currentModules})

			.success(function (data)
			{
				if (data.success)
				{
					var content = data.html,
						blob    = new Blob([content], {type : 'text/html'}),
						link    = angular.element('<a></a>');

					link.attr('href', window.URL.createObjectURL(blob));
					link.attr('download', data.fileName);
					link[0].click();
				}
			})
			.error(function (data)
			{
				console.log(data);
			})
	};

	$scope.archive = function ()
	{
		$http.post('api/email/' + $routeParams.id + '/download', {template: $scope.email.template, modules : $scope.currentModules})

			.success(function (data)
			{
				$window.open('/file/' + data.id);
			})
			.error(function (data)
			{
				console.log(data);
			});
	};	

	$scope.save = function ()
	{
		$http.post('api/email/' + $routeParams.id + '/save', {modules : $scope.currentModules})

			.success(function (data)
			{
				alert('Saved To Database');
			})
			.error(function (data)
			{
				console.log(data);
			})
	};

	$scope.$watch('files', function ()
	{
		$scope.upload($scope.files);
	});

    $scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                $upload.upload({
                    url: 'api/image-upload',
                    fields: {emailId : $scope.email._id},
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                }).success(function (data, status, headers, config) {

                	$('#available-modules').before('<img src="'+data.img+'" />');
                });
            }
        }
    };

}
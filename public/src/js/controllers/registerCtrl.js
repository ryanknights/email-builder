'use strict'

angular.module('emailBuilder').controller('registerCtrl', registerCtrl);

function registerCtrl ($scope, $http, $location, AccountService, AuthService)
{
	$scope.register = function (username, email, password)
	{	
		AccountService.register(username, email, password)

			.success(function (data)
			{
				AuthService.logIn(username, password)

					.then(function ()
					{
						$location.path('/');
					});			
			})
			.error(function (data)
			{
				alert(data);
			});
	}
}
"use strict"

angular.module('emailBuilder').controller('templatesCtrl', templatesCtrl);

function templatesCtrl ($scope, $http, $window, Templates)
{
	$scope.currentTemplates   = Templates.currentTemplates;
	$scope.availableTemplates = Templates.availableTemplates;

	$scope.addTemplate = function ()
	{
		$http.post('/api/admin/templates', {template : $scope.selectedTemplate})

			.success(function (data)
			{	
				if (!data.error)
				{
					$scope.currentTemplates = data.currentTemplates;	
				}
				else
				{
					alert(data.error);
				}
				
			})
			.error(function (data)
			{
				console.log(data);
			});
	}
}
'use strict'

angular.module('emailBuilder').controller('applicationCtrl', applicationCtrl);

function applicationCtrl ($scope, $rootScope, $location, AuthService, CurrentUserService)
{	
	$scope.currentUser = null;
	$scope.isLoggedIn  = false;

	$scope.$on('auth-login-success', function (event)
	{	
		console.log('Event => auth-login-success');
		$scope.isLoggedIn  = true;
		$scope.currentUser = CurrentUserService.getUser();
	});

	$scope.$on('auth-login-reauthenticate-success', function (event)
	{	
		console.log('Event => auth-login-reauthenticate-success');
		$scope.isLoggedIn  = true;
		$scope.currentUser = CurrentUserService.getUser();
	});	

	$scope.$on('auth-login-failed', function (event)
	{	
		$scope.currentUser = null;
		$scope.isLoggedIn  = false;
		console.log('Event => auth-login-failed');
	});

	$scope.$on('auth-logout-success', function (event)
	{
		$scope.currentUser = null;
		$scope.isLoggedIn  = false;
		console.log('Event => auth-logout-success');
	});

	$scope.$on('auth-not-authenticated', function (event)
	{
		$scope.currentUser = null;
		$scope.isLoggedIn  = false;
		console.log('Event => auth-not-authenticated');
	});

	$scope.$on('auth-not-authorized', function (event)
	{
		console.log('Event => auth-not-authorized');
	});

	$scope.logOut = function ()
	{
		AuthService.logOut();
		$location.path('/login');	
	}
}
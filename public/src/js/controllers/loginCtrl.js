"use strict"

angular.module('emailBuilder').controller('loginCtrl', loginCtrl);

function loginCtrl ($scope, $rootScope, $location, AuthService)
{
	$scope.logIn = function (username, password)
	{
		AuthService.logIn(username, password)
		
			.then(function ()
			{
				$location.path('/');
			});
	}
}
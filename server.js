"use strict";

var express 		= require('express'),
	app     		= express(),
	bodyParser 		= require('body-parser'),
	methodOverride 	= require('method-override'),
	mongoose   		= require('mongoose'),
	expressJwt      = require('express-jwt'),
	jwtSecret       = require('./app/config/secret');

var db = require('./app/config/db');

var port = process.env.PORT || 8080;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : true}));
app.use(methodOverride('X-HTTP-Method-Override'));

app.use(express.static(__dirname + '/public'));

/**
* Standard Authenticated User Routes
**/

app.use('/api/authenticate', expressJwt({secret : jwtSecret.secret}));
app.use('/api/authenticate', require('./app/routes/authenticate'));

app.use('/api/login', require('./app/routes/login'));
app.use('/api/register', require('./app/routes/register'));

app.use('/api/emails', expressJwt({secret : jwtSecret.secret}));
app.use('/api/emails', require('./app/routes/emails'));

app.use('/api/email', expressJwt({secret : jwtSecret.secret}));
app.use('/api/email', require('./app/routes/email'));

app.use('/api/image-upload', require('./app/routes/image-upload'));

/**
* Admin Authorised User Routes
**/

app.use('/api/admin/templates', expressJwt({secret : jwtSecret.secret}));
app.use('/api/admin/templates', require('./app/routes/admin/templates'));

app.use('/api/admin/users', expressJwt({secret : jwtSecret.secret}));
app.use('/api/admin/users', require('./app/routes/admin/users'));

app.use('/', require('./app/routes/default'));

app.listen(port);

console.log('App listening on port ' + port);

exports = module.exports = app;
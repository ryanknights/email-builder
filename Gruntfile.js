module.exports = function (grunt)
{
	grunt.initConfig(
	{
		browserify :
		{
			dist :
			{
				options :
				{
					transform :
					[
						['babelify', { loose: 'all'}]
					]
				},
				files :
				{
					'./public/assets/js/app.js' : ['./public/src/js/app.js']
				}
			}
		},
		concat :
		{
			options :
			{
				seperator : ';\n'
			},
			libs :
			{
				src  : ['./public/src/libs/angular.min.js', './public/src/libs/*.min.js'],
				dest : './public/assets/js/libs.js'
			}
		},
		ngAnnotate :
		{
			options :
			{
				singleQuotes : true
			},
			app :
			{
				files :
				{
					'./public/assets/js/app.js' : ['./public/assets/js/app.js']
				}
			}
		},
		uglify :
		{
			app :
			{
				files :
				{
					'./public/assets/js/app.min.js' : ['./public/assets/js/app.js']
				}
			},
			libs :
			{	
				options :
				{
					preserveComments : 'all',
				},				
				files :
				{
					'./public/assets/js/libs.min.js' : ['./public/assets/js/libs.js']
				}
			}
		},
		less :
		{	
			options :
			{
				compress: true
			},
			dist :
			{
				files :
				{
					'./public/assets/css/master.css' : './public/src/css/master.less'
				}
			}
		},
		watch :
		{
			scripts :
			{
				files : ['./public/src/js/**/*.js'],
				tasks : ['browserify', 'ngAnnotate:app', 'uglify:app']
			},
			styles :
			{
				files : ['./public/src/css/**/*.less'],
				tasks : ['less']
			}
		}
	});

   grunt.loadNpmTasks('grunt-browserify');
   grunt.loadNpmTasks('grunt-contrib-watch');
   grunt.loadNpmTasks('grunt-contrib-uglify');
   grunt.loadNpmTasks('grunt-contrib-concat');
   grunt.loadNpmTasks('grunt-ng-annotate');
   grunt.loadNpmTasks('grunt-contrib-less');

   grunt.registerTask('default', ['watch']);
   grunt.registerTask('build', ['browserify', 'ngAnnotate:app', 'uglify:app']);
   grunt.registerTask('libs', ['concat:libs', 'uglify:libs']);
}